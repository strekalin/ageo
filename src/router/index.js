import Vue from 'vue'
import Router from 'vue-router'
import Header from '@/layout/Header'
import Overview from '@/pages/Overview'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Overview',
      components: {
        default: Overview,
        header: Header
      },
      props: {
        header: {title: 'Overview'}
      }
    },
    {
      path: '/stock/:id',
      name: 'Stock',
      components: {
        default: function () {
          return import('../pages/StockDetails.vue')
        },
        header: Header
      },
      props: {
        header: {title: 'Stock details'}
      }
    },
    {
      path: '/row/:id',
      name: 'Row',
      components: {
        default: function () {
          return import('../pages/RowDetails.vue')
        },
        header: Header
      },
      props: {
        header: {title: 'Row details'}
      }
    },
    {
      path: '/location/:code',
      name: 'Location',
      components: {
        default: function () {
          return import('../pages/LocationDetails.vue')
        },
        header: Header
      },
      props: {
        header: {title: 'Location details'}
      }
    },
    {
      path: '/product/:id',
      name: 'Product',
      components: {
        default: function () {
          return import('../pages/ProductDetails.vue')
        },
        header: Header
      },
      props: {
        header: {title: 'Product details'}
      }
    }
  ]
})
