import Vue from 'vue'
import Vuex from 'vuex'
import overview from './modules/overview'
import messaging from './modules/messaging'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    overview,
    messaging
  }
})
