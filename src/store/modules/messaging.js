export const SHOW_ERROR = 'ADD_USER_ERROR_MESSAGE'

const state = {}
const getters = {}
const mutations = {
  [SHOW_ERROR]: async (context, payload) => {}
}
const actions = {}

export default {
  state,
  getters,
  mutations,
  actions,
  SHOW_ERROR
}
