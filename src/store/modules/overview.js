import axios from 'axios'
import { API_URL } from '../../.env'
import Messaging from './messaging'

const SET_LOADING = 'SET_OVERVIEW_LOADING'
const SET_DATA = 'SET_OVERVIEW_DATA'
const LOAD = 'LOAD_OVERVIEW'
const LOAD_STOCK = 'LOAD_STOCK'

const state = {
  isOverviewLoading: false,
  totalProducts: 0,
  totalCategories: 0,
  totalStocks: 0,
  categories: [],
  stocks: []
}

const getters = {
  isOverviewLoading: state => {
    return state.isOverviewLoading
  },
  totalProducts: state => {
    return state.totalProducts
  },
  totalCategories: state => {
    return state.totalCategories
  },
  totalStocks: state => {
    return state.totalStocks
  },
  categories: state => {
    return state.categories
  },
  categoriesFields: state => {
    return state.categoriesFields
  },
  stocks: state => {
    return state.stocks
  },
  stockFields: state => {
    return state.stockFields
  },
  stockById: (state) => (id) => {
    return state.stocks.find(stock => stock.id === id)
  }
}

const mutations = {
  [SET_LOADING] (state, payload) {
    state.isOverviewLoading = payload
  },
  [SET_DATA] (state, payload) {
    const productCounts = {}
    const stocks = payload.data.stocks

    state.totalProducts = payload.data.total_products
    state.totalCategories = payload.data.total_categories
    state.totalStocks = payload.data.total_stocks

    payload.data.categories.forEach(category => {
      let total = 0
      for (let stockId in category.stocks) {
        const totalProducts = parseInt(category.stocks[stockId].total_products)
        productCounts[stockId] = (productCounts[stockId] || 0) + totalProducts
        total += totalProducts
        category['stock_' + stockId] = totalProducts
      }
      category.total = total
    })
    stocks.forEach(stock => {
      const stockId = stock.id
      stock.total_products = productCounts[stockId] || 0
    })
    state.stocks = stocks
    state.categories = payload.data.categories
  }
}

const actions = {
  [LOAD]: async (context, payload) => {
    context.commit(SET_LOADING, true)
    const url = API_URL + '/summary'
    const promise = axios.get(url, {})
    return promise.then((data) => {
      context.commit(SET_DATA, data)
      return data
    }).catch(() => {
      context.commit(Messaging.SHOW_ERROR, 'Unable to load overview data')
    }).then(() => {
      context.commit(SET_LOADING, false)
    })
  },
  [LOAD_STOCK]: async (context, payload) => {
    let stock = context.getters.stockById(payload)
    if (!stock) {
      await context.dispatch(LOAD)
      stock = context.getters.stockById(payload)
    }
    return stock
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  SET_LOADING,
  SET_DATA,
  LOAD,
  LOAD_STOCK
}
